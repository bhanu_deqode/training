function flattenObject(ob){

    var flattenObj = {};

    for (var i in ob) {
        if (!ob.hasOwnProperty(i)) continue;

        if ((ob[i] instanceof Object)) {
            let flatObject = flattenObject(ob[i]);
            for (let x in flatObject) {
                if (!flatObject.hasOwnProperty(x)) continue;

                flattenObj[i + '.' + x] = flatObject[x];
            }
        }

        else if(ob[i] instanceof Array){
            for(var index=0;i<ob[i].length;i++){
                if(ob[i][index] instanceof Object){
                    let flatObject = flattenObject(ob[i][index]);
                    for (let x in flatObject) {
                        if (!flatObject.hasOwnProperty(x)) continue;

                        flattenObj[i + '.' + x] = flatObject[x];
                    }
                }
                else{
                    flattenObj[i + '.' + index] = ob[i][index];
                }
            }
        }

        else {
            flattenObj[i] = ob[i];
        }
    }
    return flattenObj;
}