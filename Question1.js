function secondLargest(array) {
    var max=Number.MIN_VALUE;
    var sec_max=Number.MIN_VALUE;
    for(var i=0;i<array.length;i++){
      if(max<array[i]){
          sec_max=max;
          max=array[i];
      }
      else if(sec_max<array[i]){
          sec_max=array[i];
       }
     }
    return sec_max;
}